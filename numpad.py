
from typing_extensions import Self
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk
from models import get_user_verfied


screen = Gdk.Screen.get_default()
provider = Gtk.CssProvider()
provider.load_from_path("templates/style.css")
Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)



class Num_Handler:


    def __init__(self, _id, _self):
        self.Cam_Id = _id
        self.Parent = _self
        builder = Gtk.Builder()
        builder.add_from_file("templates/num.glade")
        

        self.window = builder.get_object("numWindow")

        builder.connect_signals(self)
       
        self.textfield = builder.get_object("entry_field")
        self.execute = {
           
        }
        self.errorMsg = "Only numbers allowed and only one operator per \
calculation allowed." 
        self.window.show_all()
        self.window.set_keep_above(True) 
        self.window.set_position ( Gtk.WindowPosition.CENTER_ALWAYS)
       



    def on_destroy(self, *args):
        del self
    
    def btn_clear(self, btton):
        self.textfield.set_text("")
        

    def btn_clicked(self, button):
        """ Add the label of the pressed button to the text of 'textfield'. """
        value = button.get_label()
        text = self.textfield.get_text()
        self.textfield.set_text(text + value)

    def btn_result(self, button):
        """ Parse the text of 'textfield' and call methods accordingly. """
        text = self.textfield.get_text()
        if(1==get_user_verfied( self.Cam_Id  , text)):
         print("verfied ")
         self.Parent.Validate_User(1)
        """ else:  
         self.Parent.show_menu(0) """

        self.window.close()
         #self.window.destoy()

    def on_focus_out(self, widget, direction):

        print("Num Pad Foucs Out") 
       # self.Parent.show_menu(3)
        self.window.close()     
    
    def is_numeric(self, number):
        """ Check, if given string can be converted to a number.
            Since there is no builtin function, we have to do it on our own.
            This is the recommended way to do it. """
        try:
            float(number)
        except ValueError:
            return False
        return True


