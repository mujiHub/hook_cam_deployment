from onvif import ONVIFCamera

class camONVIF():
    def __init__(self, ip, usr, pwd):
        self.usr = usr
        self.pwd = pwd
        self.cam = ONVIFCamera(ip, 80, usr, pwd)
        self.ptz = self.cam.create_ptz_service()
        self.media = self.cam.create_media_service()
        self.imaging = self.cam.create_imaging_service()
        self.profile = self.cam.media.GetProfiles()[0]

    def get_ptz_status(self):
        
        request = self.cam.ptz.create_type('GetStatus')
        request.ProfileToken = self.profile.token
        status = self.cam.ptz.GetStatus(request)
        return {'pan': status.Position.PanTilt.x, 'tilt': status.Position.PanTilt.y, 'zoom': status.Position.Zoom.x}

    def set_home(self):
        
        request = self.cam.ptz.create_type('SetHomePosition')  # 设置当前位置为原点位置
        request.ProfileToken = self.profile.token
        self.cam.ptz.SetHomePosition(request)

    def move_home(self):
       
        request = self.cam.ptz.create_type('GotoHomePosition')  # 回到原点位置
        request.ProfileToken = self.profile.token
        if request.Speed is None:  # 设置归位的速度
            request.Speed = self.cam.ptz.GetStatus({'ProfileToken': self.profile.token}).Position
        self.cam.ptz.GotoHomePosition(request)

    def abs_move(self, pan, tilt, zoom):
       
        request = self.ptz.create_type('AbsoluteMove')
        request.ProfileToken = self.profile.token
        request.Position = {'PanTilt':{'x':pan, 'y':tilt}, 'Zoom':zoom}
        request.Speed = {'PanTilt':{'x':1, 'y':1}, 'Zoom':1}    # default speed
        self.ptz.AbsoluteMove(request)

    def continue_move(self, speed_pan, speed_tilt, speed_zoom):
      
        request = self.ptz.create_type("ContinuousMove")
        request.ProfileToken =  self.profile.token
        request.Velocity = {'PanTilt':{'x':speed_pan, 'y':speed_tilt}, 'Zoom':speed_zoom}
        self.ptz.ContinuousMove(request)

    def relative_move(self, pan, tilt, zoom):
       
        request = self.cam.ptz.create_type('RelativeMove')
        request.ProfileToken = self.profile.token
        request.Translation = {'PanTilt':{'x':pan, 'y':tilt}, 'Zoom':zoom}
        request.Speed = {'PanTilt':{'x':1, 'y':1}, 'Zoom':1}
        self.ptz.RelativeMove(request)

    def stop_move(self):
        
        self.ptz.Stop({'ProfileToken': self.profile.token})

    def snap_image(self, path):
       
        import requests, os
        res = self.media.GetSnapshotUri({'ProfileToken': self.profile.token})
        auth = requests.auth.HTTPDigestAuth(self.usr, self.pwd)
        response = requests.get(url=res.Uri, auth=auth)
        path_out = os.path.join(path, 'tmp.jpg')
        with open(path_out, 'wb') as fp:
            fp.write(response.content)

    def get_rtsp(self):
        
        obj = self.media.create_type('GetStreamUri')
        obj.StreamSetup = {'Stream': 'RTP-Unicast', 'Transport': {'Protocol': 'RTSP'}}
        obj.ProfileToken = self.profile.token
        res_uri = self.media.GetStreamUri(obj)['Uri']
        return res_uri

    def get_image_status(self):
        
        request = self.imaging.create_type('GetStatus')
        request.VideoSourceToken = self.profile.VideoSourceConfiguration.SourceToken
        return self.imaging.GetStatus(request)

    def abs_move_image(self, pos, speed=1):
       
        request = self.imaging.create_type('Move')
        request.VideoSourceToken = self.profile.VideoSourceConfiguration.SourceToken
        request.Focus = {'Absolute': {'Position': pos, 'Speed': speed}}
        self.imaging.Move(request)

    def continue_move_image(self, speed=1):
        
        request = self.imaging.create_type('Move')
        request.VideoSourceToken = self.profile.VideoSourceConfiguration.SourceToken
        request.Focus = {'Continuous':{'Speed':0.5}}
        self.imaging.Move(request)

    def relative_move_image(self, dist, speed=1):
        
        request = self.imaging.create_type('Move')
        request.VideoSourceToken = self.profile.VideoSourceConfiguration.SourceToken
        request.Focus = {'Relative': {'Distance':dist, 'Speed':speed}}
        self.imaging.Move(request)

    def stop_move_image(self):
       
        self.imaging.Stop({'VideoSourceToken': self.profile.VideoSourceConfiguration.token})

if __name__ == "__main__":
    pass
