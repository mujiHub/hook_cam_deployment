import cairo
import gi
from sqlalchemy import true

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')


from gi.repository import Gtk,  WebKit2, Gdk, GLib

from app_settings import *
from msg_camera import *
from numpad import Num_Handler


class Rotate(Gtk.Window):
    def __init__(self, _parent):
        Gtk.Window.__init__(self)
        self.parent = _parent
        self.User_Verfied = False
        self.disp_lock = False
        self.wind_web = None
        self.set_size_request(30, 20)

        self.connect('destroy', self.onDestroy)
        self.connect('draw', self.draw)
        self.image = Gtk.Image()
        self.image.set_from_file("templates/rotate.png")
        eventbox = Gtk.EventBox()        
        eventbox.connect("button-press-event", self.on_rotate_press)
        self.add(eventbox)
        eventbox.add(self.image)     
        self.timeout_id = GLib.timeout_add(6000, self.on_timeout, None)

        self.image.connect('touch-event', self.touched)
        self.image.connect('focus-out-event', self.on_focus_out)
        self.image.connect('focus-in-event', self.touched)
        self.set_keep_above(True)

        
        hw = getScreensize() 
       # print(hw[0])
        #print("\n")
        #print(hw[1])
        #self.move(120, 100)
        self.move(hw[0]-250, hw[1]-180)
        
        self.set_decorated (False)
        self.set_keep_above(True) 
        


        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)

        self.set_app_paintable(True)
        self.show_all()
 ####################################################################################### event
    def touched(self, widget, ev):
            print(" TOUCHED ICON  focus int ")
            self.disp_lock = True

    def on_focus_out(self, widget, direction):
         print(" it is slider focus out ")
         self.disp_lock = False  

    def on_timeout(self, user_data):
         
        if(self.disp_lock is True ):           
          return True
        self.parent.msg_from_gear(0)
        self.close()              

 #######################################################################################



    def draw(self, widget, context):
        context.set_source_rgba(0, 0, 0, 0)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)

    def on_rotate_press(self, widget, event):
       
        try:

            return msg_cam("Flip")  
       
        except EOFError:

          return "ERROR"     
             
       

######################################
         


    
                        
    def onDestroy(self, *args):
        self.parent.msg_from_rotate(0)
######################################################show web settings 


             
   
