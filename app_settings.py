from ast import Return
from gi.repository import Gtk,  Gdk
import argparse
#import ffmpeg
import sys
import os
import os.path
from models import *
import shutil
import time

import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst

########################################## These are configurable Settings that application read on Boot  #########################################################################

CAM_ID = 3  # Id of camera in Db

Clip_Length =30  # Length of clips in multiple of seconnds .

Delete_After_Gbs = 1   # maximum Storage to Hold Recordings 

Storage_Dir_Path = "/home/muji/Staged/recordingsE/"  # This is where System will store file if directory not exist it will pick default path 
                     # what ever folder is path is given video folder and snap folder will be created in side 


Usb_Mount_Path ="/media/viewer/74AF-C78E/"  # mount path of USB folder 





#################################################################### end of user settings ######################################################

Slider_Value =0
ls = None


def set_slider_value(value):
     global Slider_Value   #declare a to be a global
     Slider_Value = value  #this sets the global value of a

def get_slider_value():
     
      return Slider_Value 
############################################# -----get working folder and make media folders 
def create_folders():

       if(os.path.isdir(Storage_Dir_Path)):
        if(os.path.exists(Storage_Dir_Path+"videos") == False):
             os.mkdir(Storage_Dir_Path+"videos")
             os.mkdir(Storage_Dir_Path+"videos/thumbnails")
             os.mkdir(Storage_Dir_Path+"snaps")


             update_paths (CAM_ID, Storage_Dir_Path+"videos",  Storage_Dir_Path+"snaps", os.getcwd()+"/hook.png", Clip_Length, Delete_After_Gbs)  
             return

       global ls

       ls = [os.getcwd()+"/static/videos", os.getcwd()+ "/static/videos/thumbnails", os.getcwd()+"/static/snaps" ]

       for folder in ls:
     
        
        if os.path.isdir(folder):
            print("Exists")
        else:
                print("Doesn't exists")
                os.mkdir(folder)
       update_paths (CAM_ID, ls[0],ls[2], os.getcwd()+"/hook.png", Clip_Length, Delete_After_Gbs)     



############################################# Enf 
def getScreensize():
        screen = Gdk.Display.get_default()
        w=0
        h=0
        for x in range(0, screen.get_n_monitors()):
            w += screen.get_monitor(x).get_geometry().width
            if ( h < screen.get_monitor(x).get_geometry().height ):
                h = screen.get_monitor(x).get_geometry().height
        #print (w, ' ', h)
        return [w,h]

#############################################

def copy_files(_list, ):
    if(os.path.exists(Usb_Mount_Path) == False):
        print("usb path not mounted ")
        return False


    try:

        for file_n in _list:
            ret_dest = shutil.copy2(file_n, Usb_Mount_Path)
            print("File copied successfully.")
            return ret_dest
        # If source and destination are same
    except shutil.SameFileError:
        print("Source and destination represents the same file.")
    
    # If there is any permission issue
    except PermissionError:
        print("Permission denied.")
    
    # For other errors
    except:
        print("Error occurred while copying file.")    

#############################################
#gst-launch-1.0 filesrc location=test.mp4 ! decodebin ! videoconvert ! pngenc snapshot=true ! filesink location=frame.png
def generate_thumbnail(in_filename, out_filename, time, width):
    
    img_extract_line =" filesrc "+  in_filename +" ! decodebin ! videoconvert ! jpegenc snapshot=true ! filesink location="+out_filename
    pipe= Gst.parse_launch(img_extract_line )
    try:
      
           pipe.set_state(Gst.State.PLAYING)
           time.sleep(3)
           pipe.set_state(Gst.State.NULL)
       
    except :
        print("thumbnail error ")



    """ 
def generate_thumbnail(in_filename, out_filename, time, width):
    try:
        (
            ffmpeg
            .input(in_filename, ss=time)
            .filter('scale', width, -1)
            .output(out_filename, vframes=1)
            .overwrite_output()
            .run(capture_stdout=True, capture_stderr=True)
        )
    except ffmpeg.Error as e:
        print(e.stderr.decode(), file=sys.stderr)
        sys.exit(1) """

#############################################


def delete_by_ids(_ids):

       for id in _ids:
        print(id)
        row = get_clip_by_id(id)
        if( row):
          # delet in db
          
          path_del=  row[3]
          delete_file(path_del, id)

          # in case of video delete is thumbnail also 
          if(row[4] ==1):
            dirpath = os.path.dirname(os.path.realpath(path_del ))
            # delete thumbnail
            thunmb_name = os.path.splitext(os.path.basename(path_del))[0]
            thunmb_path = dirpath+"/thumbnails/"+thunmb_name+".jpg"
            delete_file( thunmb_path, -1)
          
          
         
#############################################

def delete_file(_filename,_db_id):
            # Handle errors while calling os.ulink()
        try:
            os.remove(_filename)
            if(_db_id is not -1):
              delete_clip(_db_id) 
        except:
            print("Error while deleting file")  

######################################################################
""" def main():
    

    create_folders()
    # copy_files(["/home/muji/Staged/HookCam_stage/static/videos/cam_pi_3_792022-369.mp4"], usb_mount_path)

if __name__ == '__main__':
     main()                           """
