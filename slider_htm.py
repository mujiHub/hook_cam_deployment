
import cairo
import gi

from app_settings import *

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')


from gi.repository import Gtk,   Gdk, GLib


class Slider(Gtk.Window):
    def __init__(self, _parent,_web_view):
        Gtk.Window.__init__(self)

        self.parent = _parent               
        self.disp_lock = False  
        
      

       
        hw=getScreensize()
       
            
        


        #wBox = builder.get_object("webbox")
        box = Gtk.Box(spacing=350)
       # box.set_opacity(0)   

 
       
        scrolled_window = Gtk.ScrolledWindow()		# scrolling window widget
        #scrolled_window.set_opacity(0.1)
        scrolled_window.add(_web_view)
      
        box.pack_start(scrolled_window, True, True, 0)
        
        self.add(box)
        self.set_default_size(600, 200)
        self.move(hw[0]-1150, hw[1]-300)
        self.set_decorated (False)
        self.set_keep_above(True) 

        self.connect('destroy', self.onDestroy)
      
        self.connect('draw', self.draw)
      
        self.connect('focus-in-event', self.touched)
        
        self.connect('focus-out-event', self.on_focus_out)
        
        
        self.timeout_id = GLib.timeout_add(6000, self.on_timeout, None)
      

      
       

       

        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)

        self.set_app_paintable(True)
        
        """  scrolled_window.set_opacity(1)
        self.set_opacity(1)
        box.set_opacity(1)
        _web_view.set_opacity(0.5) """
        self.show_all()

    def touched(self, widget, ev):
        print(" TOUCHED slider focus in ")
        self.disp_lock = True

    def on_focus_out(self, widget, direction):
         print(" it is slider focus out ")
        
         self.close()
         self.parent.msg_from_slider(0)  

    def on_timeout(self, user_data): 

        if(self.disp_lock is False):    
             
             self.close()
        return True     



    def draw(self, widget, context):
        context.set_source_rgba(0, 0, 0, 0)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)


  
        
    def onDestroy(self, *args):
        
        self.close()
        self.parent.msg_from_slider(0)
