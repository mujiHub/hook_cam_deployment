from pickle import FALSE
import subprocess
import gi
from sqlalchemy import false
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk, GObject
gi.require_version('Gst', '1.0')
from models import *
import threading
import time
import multiprocessing
import os

from gi.repository import Notify


from camera import Camera



    
from app_settings import *
from models import get_is_zoom
from msg_slider import *




 

#########################################################
camObj = None
b_EXIT = False 
pipe_from_web = None

#NumPad = None
Verfied = False
Slider_Obj  = None
#Gear_Obj  = None

Main_Window = None
Miss_it =True    

Proc_socket = None

def setEXIT(value):
     global b_EXIT   #declare a to be a global
     b_EXIT = value  #this sets the global value of a

class Handler():
   
    
    
    
    def onDestroy(self, *args):
        camObj.close()
        if(Proc_socket is not None):
            Proc_socket.kill() 
        
        Gtk.main_quit()
        
        
    def onDestroyPop(self, *args):
        Gtk.main_quit()

    

    """     def on_scale_button_changed(self, scalebutton, value):
        print("ScaleButton value: %0.2f" % (value))
     """

    def  onRealize_player(self, _widget):

          global  camObj
          print("on Realized is called ")
         
          Proc_socket = subprocess.Popen('./run_slider.sh', shell=True, stdin=subprocess.PIPE)
          live_xid=_widget.get_property('window').get_xid()
        
          
          camObj=Camera(CAM_ID, live_xid )
          self.hide_controls()

       
            


          
    def on_touch_event(self, widget, ev): 
         
         
            self.show_controls()
           
          
         
            
    def hide_controls(self):

         if(get_is_zoom(CAM_ID)==1):
            send_to_slider("Hide")
        

    def show_controls(self):
      

         if(get_is_zoom(CAM_ID)==1):
            send_to_slider("Show") 
            GObject.timeout_add(18000, self.hide_controls)  
    
              

    
        
    def on_focus_in(self, widget, direction): 
      
        self.show_controls()  
      
  
        
        
        

    def on_key_press_event(self, widget, event):

        # see if we recognise a keypress  GDK_KEY_F9
        
       
        if event.keyval == Gdk.KEY_F9:
            Main_Window.unfullscreen()
           
            Main_Window.show_all()

        if event.keyval == Gdk.KEY_F10:
            Main_Window.fullscreen()
           
            Main_Window.show_all()    







""" function enable() {
    global.stage.get_actions().forEach(a => a.enabled = false);

    let disableUnfullscreenGesture = () => {
        global.stage.get_actions().forEach(a => { if (a != this) a.enabled = false;});
    }
    global.display.connect('notify::focus-window', disableUnfullscreenGesture);
    global.display.connect('in-fullscreen-changed', disableUnfullscreenGesture);
} 
 """
########################################################-- start Part

def run_function( ):
       
    global   Main_Window
 
   
    print("Cam Runner ")
    builder = Gtk.Builder()
    builder.add_from_file("templates/main.glade")
    eventHandler = Handler()
    builder.connect_signals(eventHandler)
   
    Main_Window = builder.get_object("main_win")
    drawing_area = builder.get_object("video_canvas")
    drawing_area.add_events(Gdk.EventMask.TOUCH_MASK)
    drawing_area.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
    drawing_area.connect('button_press_event', eventHandler.on_touch_event)
    drawing_area.connect('touch-event', eventHandler.on_touch_event)
    drawing_area.connect('realize', eventHandler.onRealize_player)
    drawing_area.set_double_buffered(False)
    #
    #https://athenajc.gitbooks.io/python-gtk-3-api/content/gtk-group/gtkscalebutton.html

   
    lst=getScreensize()
    #window.set_default_size(lst[0], lst[1])
    Main_Window.set_default_size(600, 400)

    Main_Window.fullscreen()
   
    Main_Window.set_keep_above(True)
    Main_Window.show_all()
    Gtk.main()
    
       

def thread_status(name):

    while not b_EXIT:
        rows=get_all_status()
        for i, row in enumerate(rows):
        # print (row)   
         time.sleep(5)
##################################################Thread to listen on pipe from web API #################################




if __name__ == "__main__":
    create_folders()
   
    x = threading.Thread(target=thread_status, args=(5,))
    from webprocess import run_api
    p2 = multiprocessing.Process(target=run_api, args=( ))

   
    p2.start()
  
    p2.join(0.25)       
    print("Cam Closed ") 
    
    x.start()
    run_function()