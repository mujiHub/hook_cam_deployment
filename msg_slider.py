import socket
import threading
import time

Socket_obj = None


def snif_message(_self):
      while _self.message.lower().strip() != 'bye':
        _self.client_socket.send(_self.message.encode())  # send message
        data = _self.client_socket.recv(1024).decode()  # receive response
        print('Received from server: ' + data)  # show in terminal
        time.sleep(1)
      _self.client_socket.close()   
        


            ####################################-----class
class sck_client():

    def __init__(self,):
        print("")
        host = socket.gethostname()  # as both code is running on same pc
        port = 5000  # socket server port number
        self.client_socket = socket.socket()  # instantiate
        self.client_socket.connect((host, port))  # connect to the server
        self.message ="Hide"
        self.loopTh= threading.Thread(target=snif_message, args=(self,), daemon=True)
        self.loopTh.start()
        
    def send_msg( self, _msg):
       self.message =_msg
       if(self.client_socket ):
        self.client_socket.send(_msg.encode())  # send message  

        


def send_to_slider(_msg):

    global Socket_obj, Visible



    if(Socket_obj is None):
        Socket_obj= sck_client() 
    Socket_obj.send_msg(_msg)    




""" if __name__ == '__main__':
   obj= sck_client() 
   while True:
    time.sleep(2)
    obj.send_msg("Show")
    time.sleep(2)
    obj.send_msg("Hide")
   msg= "Hide"
  # obj.loop()
   obj.send_msg("Show")
   #obj.client_socket .send(msg.encode()) 
   obj.send_msg("Hide") """