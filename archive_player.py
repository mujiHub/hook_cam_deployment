import gi
import sys
gi.require_version("Gtk", "3.0")
gi.require_version('Gst', '1.0')
gi.require_version('GstVideo', '1.0')
from gi.repository import Gtk, Gst, GLib, GstVideo
from pathlib import Path
from gi.repository import Gdk
from playerB import Player
from models import get_clip_by_id
import time


screen = Gdk.Screen.get_default()
css_provider = Gtk.CssProvider()
css_provider.load_from_path("templates/style.css")
context = Gtk.StyleContext()
context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER)
# This is the refresh time of slider in milliseconds
SLIDER_REFRESH = 1000


class PlayerUI:
    def __init__(self, _uri):
        # Use Glade Template 
        builder = Gtk.Builder()
        builder.add_from_file("templates/player.glade")
        builder.connect_signals(self)
        
        # Initialize Player Backend
        self.player = Player(self)
         # adding gtksink to pipeline for video output
        self.gtksink = Gst.ElementFactory.make("gtksink")

        self.player.playbin.set_property("video-sink", self.gtksink)

        # main window
        self.window = builder.get_object("main_win")


      
     




          # PlayArea
        playArea = builder.get_object("playArea")
        playArea.add(self.gtksink.props.widget)
        self.gtksink.props.widget.set_hexpand(True)
        self.gtksink.props.widget.set_vexpand(True)

         # slider seeker
        self.seeker = builder.get_object("seeker")
        self.durationText = builder.get_object("duration")
        self.playBtn = builder.get_object("playBtn")
          # icons
        self.playIco = builder.get_object("playIco")
        self.pauseIco = builder.get_object("pauseIco") 
     
        # button signals
        self.playBtn.connect("clicked", self.onPlay)
         # UI 
        self.window.set_title("Player")
        self.sliderHandlerId = self.seeker.connect("value-changed", self.onSliderSeek)
           # used for connecting video to application (Not Used as of now)
        self.player.bus.enable_sync_message_emission()
        self.player.bus.connect("sync-message::element", self.onSyncMessage)


         # show window and initialize player gui
        selected_file= Gst.filename_to_uri(_uri)
        self.player.setUri(selected_file)
       
       
        self.onPlay()
        self.window.set_default_size(800,740)

          # Player window display parameters
         #window.set_default_size(lst[0], lst[1]) 
           # Connect Signals Here
        # window signals
        self.window.connect("destroy", self.onDestroy)
        self.window.connect("focus-out-event", self.on_focus_out)
        
        self.window.set_keep_above(True)
        self.window.set_position ( Gtk.WindowPosition.CENTER_ALWAYS)
        self.window.set_decorated(False) 
        self.window.show_all()
       
        Gtk.main()

    def refreshIcons(self):
        # if song is paused show play icon else play icon
        if(self.player.status == Gst.State.PLAYING):
            self.playBtn.set_label("Pause")
            self.playBtn.set_image(self.pauseIco)
        else:
            self.playBtn.set_image(self.playIco)
            self.playBtn.set_label("Play")    

        ############       Events Area 

    def onSyncMessage(self, bus, message):

         pass

    def refreshSlider(self):  
            if self.player.status != Gst.State.PLAYING:
                return False  # cancel timeout
            else:
                
                try:
                    success, duration = self.player.getDuration()
                    d = float(duration) / Gst.SECOND
                    if not success:
                        Exception("Error Occured when fetching duration")
                    else:
                        self.seeker.set_range(0, d)
                    #fetching the position, in nanosecs
                    success, position = self.player.getPosition()
                    
                    if not success:
                        Exception("Couldn't fetch current song position to update slider")
                                    
                    # converts to seconds
                    p = float(position) / Gst.SECOND

                    durationToShow = str(time.strftime("%H:%M:%S", time.gmtime(d)))
                    postionToShow = str(time.strftime("%H:%M:%S", time.gmtime(p)))
                    self.durationText.set_label(postionToShow+"/"+durationToShow)

                    # block seek handler so we don't seek when we set_value() or else audio will break
                    self.seeker.handler_block(self.sliderHandlerId)
                    self.seeker.set_value(p)
                    self.seeker.handler_unblock(self.sliderHandlerId)
    
                except Exception as e:
                    print(e)

                return True  # continue calling every SLIDER_REFRESH milliseconds


    def on_close_clicked(self, button=None):
        self.close()

    def on_copy_clicked(self, button=None):
        print("Copyyyyyyyyyyyyyyyyyy")

    def on_archive_clicked(self, button=None):
        print("Archiveeeeee")           

    def onPlay(self, button=None):
            
            if(self.player.status != Gst.State.PLAYING):
                self.player.play()
                self.gtksink.props.widget.show()
                GLib.timeout_add(SLIDER_REFRESH, self.refreshSlider)
            else:
                self.player.pause()
            self.refreshIcons() 

    def onSliderSeek(self, slider):
         self.player.seek(self.seeker.get_value()) 

    def onStop(self, button=None):
            self.player.changeState(Gst.State.NULL)
            self.seeker.set_value(0)
            self.gtksink.props.widget.hide()
            self.refreshIcons()   

    def onDestroy(self, *args):
          print("PLAYER Destroyed ")
          self.close()   
         
    def close(self):
          self.player.stop() 
          Gtk.main_quit()
          exit()


    def on_focus_out(self, widget, direction):
         print(" it is player focus out ")
         self.window.close()

  
def main(args):
    
      #PlayerUI("/home/muji/Staged/HookCam_stage/static/videos/cam_pi_3_7212022-122718.mp4")
      
      id_passed=int(args[1])
  
      print("\n my id to Play >>>>>>>>>>>>>>  "+args[1])
      row=get_clip_by_id( id_passed)
      print("\n file_path  "+row[3])
      PlayerUI(row[3])

if __name__ == '__main__':
    
    sys.exit(main(sys.argv))   