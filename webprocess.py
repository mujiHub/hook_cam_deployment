
from fastapi import FastAPI
import uvicorn as uvicorn
from fastapi.requests import Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi import APIRouter
from models import *

import os
from onvifControl import camONVIF
from fastapi.responses import Response
from fastapi.responses import StreamingResponse, FileResponse
from typing import List
import array as arr
from app_settings import *
from archive_player import PlayerUI
from datetime import datetime,  timedelta

from urllib.parse import unquote
from msg_camera import *
##################################################_____________________________Globals 


Onvif_Handel = None

files = None







###################################################-----------------------runner----------------------####################################
def run_api():
    
    global files
    files = {
        
        item: os.path.join(getPath(), item)
        for item in os.listdir(getPath())
    }  
    
    uvicorn.run(app, host="0.0.0.0", port=10000)  
   # uvicorn.run(app, host="127.0.0.1", port=5000, reload=True)
    print("Web API running")


def getFolder():
      ret=os.path.dirname(os.path.abspath(__file__))
      return ret

def getPath():
    return get_cam_by_id(CAM_ID)[4].strip()

video_files = {
item: os.path.join(getPath(), item)
for item in os.listdir(getPath())
}
##########################################--------------------------------web API---------------------------------############################
# mount Directory 


app = FastAPI(openapi_url="/api/v1/openapi.json")
app.mount("/static", StaticFiles(directory="static"), name="static")


templates = Jinja2Templates(directory="templates")





@app.get("/loadmain/", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse("template.html", {'request': request }) 

@app.get("/slider/", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse("slider.html", {'request': request }) 

@app.get("/rotate/", response_class=HTMLResponse)
async def rotate(request: Request):

    return msg_cam("Flip") 

@app.get("/set_focus/{val}")    
async def send_to_nano(val: int ):
    
    val_calc = round(val* (0.00999),3)
    if(val_calc ==0):
         val_calc = 0.001

    global Onvif_Handel
    if(Onvif_Handel == None):
        _row=get_cam_by_id(CAM_ID)          
        Onvif_Handel =camONVIF(_row[10], _row[15] , _row[16])
    Onvif_Handel.abs_move(0, 0, val_calc )    
    return "OK"       


@app.get("/get_recordings/")
async def read_recordings():
    row = get_clips(CAM_ID)
    return  row

########################## for GTL player
""" @app.get("/get_play/{id}")
async def play(id: int):
    print("play file ")
    if(GTK_Connection is not None):
        GTK_Connection.send("Play, 15")
    return "OK" 
   # PlayerUI("/home/muji/Dev/rec_video/sample.mp4")
     """

       
    
@app.get("/get_snaps/")
async def read_snap():
    row = get_snaps(CAM_ID)
    return  row    


@app.get("/snap/")
async def snap():
   
    try:

        return msg_cam("Snap")  
       
    except EOFError:

        return "ERROR"  

@app.get("/start_record/")
async def startRecording():
    try:

        return msg_cam("Record")  
       
    except EOFError:

        return "ERROR" 
@app.get("/flip/")
async def flipView():
    try:

        return msg_cam("Flip")  
       
    except EOFError:

        return "ERROR"         
        
        

@app.get("/stop_record/")
async def stopRecording():

    try:

        return msg_cam("Stop") 
       
    except EOFError:

        return "ERROR" 
         


@app.get("/delete/{id}")
async def delet_recording(id: int):
    print("delete called "+str(id))
    deletRecording(id)

  
def deletRecording(_id):  
    row =  get_clip_by_id( id)
    if(row):
        path=row[3]
        delete_clip( id)
        os.remove(path)  


@app.get("/delete_multiple/{ids}")
async def delet_recording(ids:int):
  print("delete_multiple called "+ids)






@app.get("/copy/{id}")
async def copy_recording(id: int):
    print("copy called "+str(id)) 
    file=get_clip_by_id( id)[3]
    copy_files([file])
   


@app.get("/copy_multiple/{ids}")
async def copy_recording(ids:int):
  print("copy_multiple called"+ids)
  for id in ids:
    file=get_clip_by_id( id)[3]
    copy_files([file])







@app.get("/archive/{id}")
async def archive_recording(id: int):
    print("archive called "+str(id)) 



@app.get("/archive_multiple/{ids}")
async def archive_recording(ids: int):
    print("archive_multiple called "+ids)     



#VIRTUAL_ENV: /home/muji/Dev/env
#print(os.environ.get('HOME'))

#print(os.environ.get('VIRTUAL_ENV'))

@app.get("/play/{id}")
async def play_recording(id: int):
    print("\n play called >>>>>>>>>>>> "+str(id))  
   
    play_path = "/bin/bash -c 'cd "+os.getcwd()+"/"+" && source "+os.environ.get('VIRTUAL_ENV')+"/bin/activate && python3 archive_player.py "+str(id)+"'"

    os.system(play_path)
    #os.system("/bin/bash -c 'cd /home/muji/Staged/HookCam_stage/ && source /home/muji/Dev/env/bin/activate && python3 archive_player.py "+str(id)+"'")


@app.get("/filter_clips/{startDate}/{endDate}")

async def filter_clips(startDate: float,endDate:float):
    
    cstart_dt =   datetime.fromtimestamp(startDate / 1000.0)
    cend_dt =   datetime.fromtimestamp(endDate / 1000.0)
    print("/@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/")
    print(cstart_dt)
    print(cend_dt)


    dt_start = (cstart_dt-timedelta(minutes=1)).strftime("%Y-%m-%d %H:%M")
    dt_end =  (cend_dt +timedelta(minutes=1)).strftime("%Y-%m-%d %H:%M") 
    print("DATE >>>> +++   "+dt_start)
    print("DATE >>>> +++   "+dt_end)
    row = get_clips_bydate(  dt_start, dt_end, CAM_ID)
    return row


@app.get("/filter_snaps/{startDate}/{endDate}")
  
async def filter_snaps(startDate: float,endDate:float):
    
    cstart_dt =   datetime.fromtimestamp(startDate / 1000.0)
    cend_dt =   datetime.fromtimestamp(endDate / 1000.0)
    print("/@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/")
    print(cstart_dt)
    print(cend_dt)


    dt_start = (cstart_dt-timedelta(minutes=1)).strftime("%Y-%m-%d %H:%M")
    dt_end =  (cend_dt +timedelta(minutes=1)).strftime("%Y-%m-%d %H:%M")
    print("DATE >>>> +++   "+dt_start)
    print("DATE >>>> +++   "+dt_end)
    row = get_snaps_bydate(  dt_start, dt_end, CAM_ID)
    return row
  