import cairo
import gi
from sqlalchemy import true

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')


from gi.repository import Gtk,  WebKit2, Gdk, GLib

from app_settings import *
from numpad import Num_Handler


class Gear(Gtk.Window):
    def __init__(self, _parent):
        Gtk.Window.__init__(self)
        self.parent = _parent
        self.User_Verfied = False
        self.disp_lock = False
        self.wind_web = None
        self.set_size_request(30, 20)

        self.connect('destroy', self.onDestroy)
        self.connect('draw', self.draw)
        self.image = Gtk.Image()
        self.image.set_from_file("templates/settingicon.png")
        eventbox = Gtk.EventBox()        
        eventbox.connect("button-press-event", self.on_setting_press)
        self.add(eventbox)
        eventbox.add(self.image)     
        self.timeout_id = GLib.timeout_add(6000, self.on_timeout, None)

        self.image.connect('touch-event', self.touched)
        self.image.connect('focus-out-event', self.on_focus_out)
        self.image.connect('focus-in-event', self.touched)
        self.set_keep_above(True)

        
        hw = getScreensize() 
        self.move(hw[0]-130, hw[1]-180)
        
        self.set_decorated (False)
        self.set_keep_above(True) 
        


        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)

        self.set_app_paintable(True)
        self.show_all()
 ####################################################################################### event
    def touched(self, widget, ev):
            print(" TOUCHED ICON  focus int ")
            self.disp_lock = True

    def on_focus_out(self, widget, direction):
         print(" it is slider focus out ")
         self.disp_lock = False  

    def on_timeout(self, user_data):
         
        if(self.disp_lock is True ):           
          return True
        self.parent.msg_from_gear(0)
        self.close()              

 #######################################################################################



    def draw(self, widget, context):
        context.set_source_rgba(0, 0, 0, 0)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)

    def on_setting_press(self, widget, event):
        #self.visible(False)
        self.disp_lock = True  
        if( self.User_Verfied is False):
             self.NumPad = Num_Handler(CAM_ID, self)
             
             return
        if( self.User_Verfied is True):
                self.show_web()

######################################
    def onDestroyWeb(self, *args):
        
        
        self.wind_web = None
        self.disp_lock= False 
        self.parent.msg_from_gear(0)              

#############################called by num pad
    def  Validate_User(self, _flag):
            if(_flag==1):
             self.User_Verfied =True 
             self.show_web() 
           # else:
              #  self.visible(True) 
                

    
                        
    def onDestroy(self, *args):
        self.parent.msg_from_gear(0)
######################################################show web settings 



    def show_web(self):
            self.disp_lock = True
            if(self.wind_web):
                self.wind_web.set_keep_above(True)              
                return

            
            if(self.wind_web is not None and self.User_Verfied is True  ):
                self.wind_web.set_keep_above(True)
                return

            builder = Gtk.Builder()
            builder.add_from_file("templates/webview.glade")
        # builder.connect_signals(self.Handeler)


            self.wind_web = builder.get_object("webmain")
            hw=getScreensize()
            self.wind_web.set_default_size(hw[0]-100,hw[0]-100)
            
            self.wind_web.set_position ( Gtk.WindowPosition.CENTER_ALWAYS)

    
            wBox = builder.get_object("webbox")

            web_view = WebKit2.WebView()				# initialize webview
            web_view.load_uri('http://0.0.0.0:10000/loadmain')	# default homepage
            scrolled_window = Gtk.ScrolledWindow()		# scrolling window widget
            scrolled_window.connect('destroy', self.onDestroyWeb)
            scrolled_window.add(web_view)
            wBox.pack_start(scrolled_window, True, True, 0)
            
            self.wind_web.show_all()
            self.wind_web.set_keep_above(True)            
             
   
#TransparentWindow()
#Gtk.main()