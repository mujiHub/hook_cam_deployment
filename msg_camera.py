# echo-client.py
import sys, errno
import socket

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 65432  # The port used by the server
Sock_obj = None
Sock_resp = None 

def msg_cam(_msg):
    global Sock_obj, Sock_resp
    try:
        if(Sock_obj is None):

            Sock_obj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            Sock_obj.connect((HOST, PORT))
        Sock_obj.sendall(_msg.encode("utf-8") )
        Sock_resp = Sock_obj.recv(1024)
        Sock_obj.close() 
        Sock_obj = None
        

        return Sock_resp
    except IOError as e:
     if e.errno == errno.EPIPE:
        Sock_obj = None
        ### Handle the error ###
""" if __name__ == '__main__':
     msg_cam("Record")              """