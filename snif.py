#!/usr/bin/env python3
import sys
import gi
import logging

gi.require_version("GLib", "2.0")
gi.require_version("GObject", "2.0")
gi.require_version("Gst", "1.0")
from gi.repository import GLib, GObject, Gst

logging.basicConfig(
    level=logging.DEBUG, format="[%(name)s] [%(levelname)8s] - %(message)s"
)
logger = logging.getLogger(__name__)


class CustomData:
    def __init__(self):
        # Creat the single element playbin
        self.playbin = Gst.ElementFactory.make("playbin", "playbin")
        # Are we in playing state?
        self.playing = False
        # Should we terminate execution?
        self.terminate = False
        # Is seeking enabled for this media?
        self.seek_enabled = False
        # Have we performed the seek already?
        self.seek_done = False
        # How long does this media last, in nanoseconds?
        self.duration = Gst.CLOCK_TIME_NONE



def get_rtsp_status(_rtsp):
    # Initialize GStreamer
    # Setup internal path lists, plugins, GstRegistry
    #Gst.init(None)
    data = CustomData()
    if not data.playbin:
        logger.error("Not all elements could be created")
        sys.exit(1)
    # Set URI to play
    #data.playbin.props.uri = "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm"
    #rtsp://muji:muji@10.5.5.100/axis-media/media.amp
    #data.playbin.props.uri = "rtsp://muji:muji@10.5.5.100/axis-media/media.amp"
    data.playbin.props.uri = _rtsp
    ret = data.playbin.set_state(Gst.State.PLAYING)
    if ret == Gst.StateChangeReturn.FAILURE:
        logger.error("Unable to change state of pipeline to playing")
        sys.exit(1)
    else:
        logger.info("uri prop added")

    # Listening for messages
    # Why is a bus required?
    # Bus takes care of forwarding messages from pipeline (running in a separate thread) to the application
    # Applications can avoid worrying about communicating with streaming threads / the pipeline directly.
    #Gst.MessageType.STREAM_START
    data.bus = data.playbin.get_bus()
    while True:
        msg = data.bus.timed_pop_filtered(
            100 * Gst.MSECOND,
            Gst.MessageType.STATE_CHANGED
            | Gst.MessageType.ERROR
            | Gst.MessageType.EOS
            | Gst.MessageType.STREAM_START
            | Gst.MessageType.DURATION_CHANGED,
        )
        if msg:
            #handle_message(msg, data)
            if( msg.type == Gst.MessageType.STREAM_START):
             
               data.playbin.set_state(Gst.State.NULL)
               return 1
            if( msg.type ==  Gst.MessageType.ERROR):
            
               data.playbin.set_state(Gst.State.NULL)
               return 0 
       
          
               
      

#if __name__ == "__main__":
 #  ret= get_rtsp_status("rtsp://muji:muji@10.5.5.100/axis-media/media.amp")
  # print(ret)
