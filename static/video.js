$(document).ready(function () {
  videofunction();
  datafunctionVideo("http://0.0.0.0:10000/get_recordings/");

});
var time = document.getElementById("time");
var videoButton = document.getElementById("videoButton");
videoButton.addEventListener("click", function() {
  datafunctionVideo("http://0.0.0.0:10000/get_recordings/");
});




function videofunction() {
  document.getElementById("sensorTab").style.display = "none";
  document.getElementById("videoFilterTab").style.display = "none";
  document.getElementById("snapTab").style.display = "none";
}
//"http://192.168.100.30:10000/get_recordings/"

function getFileNameFromPath(path) {
  var n = path.lastIndexOf('/');
  return path.substring(n + 1);
}
function replacefunction(x) {
  var z = x.replace("mp4", "jpg");
  return z;
}

function datafunctionVideo(_url) {
  //alert("From data Function");
  fetch(_url)
    .then(response => response.json())
    .then(data => {
      _renderDataVideo(data);

    })
    .catch(error => {
      alert(error);
      console.log(error);
    })
}

function _renderDataVideo(data) {
  var id;
  var time;
  var url;
  var result = "";
  var sortArray = [];
  var myItem = {};
  data.forEach(element => {
    id = element[0];
    time = element[1];

    url = "http://0.0.0.0:10000/static/videos/" + getFileNameFromPath(element[3]);
    thumbnailUrl = "http://0.0.0.0:10000/static/videos/thumbnails/" + getFileNameFromPath(element[3]);
    thumbPath = replacefunction(thumbnailUrl);
    console.log(thumbPath);
    console.log(thumbnailUrl);
    // alert(thumbnailUrl);
    console.log(url);
    console.log((element[3]));
  // alert("time from get recordings:"+time);

    myItem = {
      Id: id,
      Time:time,
      
      Url: url,
      ThumbPath: thumbPath,
     
    }
    
    sortArray.push(myItem);


    console.log(myItem);
  })
  sortArray.sort(function (a, b) {
    // Turn your strings into dates, and then subtract them
    // to get a value that is either negative, positive, or zero.
    return new Date(b.date) - new Date(a.date);
  });
  console.log("---------------------------------------------------------------------");

  //console.log(sortArray);

  var reverseArray = sortArray.reverse();
  console.log(reverseArray);
  var id;
  var time;
  var url;
  var thumbPath;
  reverseArray.forEach(element => {

    console.log(element.Id);
    id = element.Id;
    time = element.Time;
    //alert("time is:"+time);
    url = element.Url;
    thumbPath = element.ThumbPath;
    console.log("#####################################");
    console.log(id);
    console.log(time);
    console.log(url);
    result += "<tr>"

    result += "<td class='py-1'>";
    result += " <input type='checkbox' class='checkMark' id='listCheck' data-id='" + id + "'>"
    result += "<img onerror=this.src='../static/no-thumbnail.jpg' src='" + thumbPath + "' alt='image'  data-toggle='modal' data-target='#playerModel'>";
    result += "</td>";
    result += "<td>";
    result += id;
    result += "</td>";
    result += "<td>";
    result += time;
    result += "</td>";
    // result += "<td>";
    //   result += path;
    //   result +=" </td>";
    result += "<td  id='" + id + "'>";
    result += "<button class='btn-lights'  onClick='playVideo(event)' data-url='" + url + "'>Play</button>";
    result += "<button class='btn-lights'id ='copybtn' onClick ='videocopyfunction(event)'>Save to Usb</button>";
    result += "<button class='btn-lights' id='archivebtn' onclick ='videoarchivefunction(event)'>Archive</button>";
    result += "</td>";
    result += "</tr>";
  })

  output.innerHTML = result;
  document.getElementById("videoTab").style.display = "block";

}

function videoFilterData() {

  var startDate = new Date(document.getElementById("videoStartDate").value);
  var endDate = new Date(document.getElementById("videoEndDate").value);


    if (startDate == "Invalid Date" || endDate == "Invalid Date") {
      return alert("Select Valid date")
    }
    else if (startDate>endDate) {
      return alert("Start Date is Greater then End date");
    }

    var utc_timestamp_start = Date.UTC(startDate.getUTCFullYear(),startDate.getUTCMonth(), startDate.getUTCDate() , 
    startDate.getUTCHours(), startDate.getUTCMinutes(), startDate.getUTCSeconds(), startDate.getUTCMilliseconds());

    var utc_timestamp_end = Date.UTC(endDate.getUTCFullYear(),endDate.getUTCMonth(), endDate.getUTCDate() , 
    endDate.getUTCHours(), endDate.getUTCMinutes(), endDate.getUTCSeconds(), endDate.getUTCMilliseconds());

    var _filter_url = 'http://0.0.0.0:10000/filter_clips/'+utc_timestamp_start+'/'+utc_timestamp_end ;
    
    datafunctionVideo(_filter_url);

}



function playVideo(event) {
  var url = $(event.target).attr("data-url");

  var idClicked = $(event.target).parent().attr('id');
  console.log(idClicked);
  //$('#camVideo source').attr('src', url);
  $('.modal-footer').attr('id', idClicked);
  //$("#camVideo").html('<source src="' + url + '" type="video/mp4; codecs="avc1.42E01E, mp4a.40.2">');
  //$("#camVideo")[0].load();
  //$('#playerModel').modal('show');

  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  idClicked = $(event.target).parent().attr('id');
  console.log(idClicked);
  var postval = 'http://0.0.0.0:10000/play/' + idClicked;
  console.log(httpGet(postval));

}


function videocopyfunction(event) {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  idClicked = $(event.target).parent().attr('id');
  console.log(idClicked);
  var postval = 'http://0.0.0.0:10000/copy/' + idClicked;
  console.log(httpGet(postval));

}






function videoarchivefunction(event) {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  var idClicked = $(event.target).parent().attr('id');
  console.log(idClicked);
  var postval = 'http://0.0.0.0:10000/archive/' + idClicked;
  console.log(httpGet(postval));

}


function videocheckfunction() {
  if ($(".myCheckbox").is(":checked")) {
    $(".checkMark").show();
    $(".checkboxButton").show();

  }
  else {
    $(".checkMark").hide();
    $(".checkboxButton").hide();
  }
}



var timer;


function myTimer() {
  console.log("timer");
  document.getElementById("recordingButton").style.display = "none";
  document.getElementById("stopButton").style.display = "block";
  document.getElementById("recTimerMaker").style.display = "block";
  //document.getElementById("time").style.display = "block";
  document.getElementById("icon").style.display = "block";
  var totalSeconds = 0;
  var second = 0;

  timer = setInterval(myTimerProgress, 1000);

  function myTimerProgress() {

    console.log("TimerProgress");
    totalSeconds += second;
    //   console.log(totalSeconds);

    hours = Math.floor(second / 3600);
    totalSeconds %= 3600;
    minutes = Math.floor(second / 60);
    seconds = second % 60;
    console.log("S" + seconds);
    console.log("M" + minutes);
    console.log("H" + hours);

    //$("time") = hours + ":"  + minutes + ":"  +  seconds;
    time.innerHTML = hours + ":" + minutes + ":" + seconds;
    console.log(time);
    second++;

  }
  var postval = 'http://0.0.0.0:10000/start_record/';
  console.log(httpGet(postval));
}

function stopTimer() {

  document.getElementById("recordingButton").style.display = "block";
  document.getElementById("stopButton").style.display = "none";
  document.getElementById("recTimerMaker").style.display = "none";
  //document.getElementById("time").style.display = "none";
  document.getElementById("icon").style.display = "none";

  clearInterval(timer);

  var postval = 'http://0.0.0.0:10000/stop_record/';
  console.log(httpGet(postval));
}

function httpGet(theUrl) {
  let xmlHttpReq = new XMLHttpRequest();
  xmlHttpReq.open("GET", theUrl, false);
  xmlHttpReq.send(null);

  return xmlHttpReq.responseText;
}


function videogetSelectedIds() {
  var checkedVals = [];
  checkedVals = $('.checkMark:checkbox:checked').map(function () {
    return $(this).attr("data-id");

  })
    .get();

  //console.log("checked values array"+ checkedVals);
  //return(checkedVals);
  //return parseInt(checkedVals);
  //var a = parseInt(checkedVals);
  //return parseInt(a);
  var arrofInt = [];
  checkedVals.map(str => {
    arrofInt.push(Number(str));
  })
  console.log(arrofInt);
  return (arrofInt);
}

function videocopyMultiple() {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  var multipleIds = (getSelectedIds());
  console.log(multipleIds);
  var postval = 'http://0.0.0.0:10000/copy_multiple/' + multipleIds;
  console.log(httpGet(postval));
  console.log(multipleIds);
}



function videodeleteMultiple() {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  var multipleIds = (getSelectedIds());
  console.log(multipleIds);
  var postval = 'http://0.0.0.0:10000/delete_multiple/' + multipleIds;
  console.log(httpGet(postval));
  console.log(multipleIds);
}


function videoarchiveMultiple() {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  var multipleIds = (getSelectedIds());
  var postval = 'http://0.0.0.0:10000/archive_multiple/' + multipleIds;
  console.log(httpGet(postval));
  console.log(multipleIds);
}

function snapShotfunction() {
  document.getElementById("app").style.display = "block";

  const FULL_DASH_ARRAY = 283;
  const WARNING_THRESHOLD = 10;
  const ALERT_THRESHOLD = 5;

  const COLOR_CODES = {
    info: {
      color: "green"
    },
    warning: {
      color: "orange",
      threshold: WARNING_THRESHOLD
    },
    alert: {
      color: "red",
      threshold: ALERT_THRESHOLD
    }
  };

  const TIME_LIMIT = 3;
  let timePassed = 0;
  let timeLeft = TIME_LIMIT;
  let timerInterval = null;
  let remainingPathColor = COLOR_CODES.info.color;

  document.getElementById("app").innerHTML = `
<div class="base-timer">
    <g class="base-timer__circle">
      <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
      <path
        id="base-timer-path-remaining"
        stroke-dasharray="283"
        class="base-timer__path-remaining ${remainingPathColor}"
        d="
          M 50, 50
          m -45, 0
          a 45,45 0 1,0 90,0
          a 45,45 0 1,0 -90,0
        "
      ></path>
    </g>
  </svg>
  <span id="base-timer-label" class="base-timer__label">

  </span>
</div>
`;

  startTimer();

  function onTimesUp() {
    clearInterval(timerInterval);
  }

  function startTimer() {
    timerInterval = setInterval(() => {
      timePassed = timePassed += 1;
      timeLeft = TIME_LIMIT - timePassed;
      document.getElementById("base-timer-label").innerHTML = (
        timeLeft
      );
      setCircleDasharray();
      setRemainingPathColor(timeLeft);

      if (timeLeft === 0) {
        console.log("Hello from snapshot");
        function httpGet(theUrl) {
          let xmlHttpReq = new XMLHttpRequest();
          xmlHttpReq.open("GET", theUrl, false);
          xmlHttpReq.send(null);

          return xmlHttpReq.responseText;
        }
        var postval = 'http://0.0.0.0:10000/snap/';
        console.log(httpGet(postval));

        document.getElementById("app").style.display = "none";

        onTimesUp();
      }
    }, 1000);
  }

  function formatTime(time) {
    const minutes = Math.floor(time / 60);
    let seconds = time % 60;

    if (seconds < 10) {
      seconds = `0${seconds}`;
    }

    return `${minutes}:${seconds}`;
  }

  function setRemainingPathColor(timeLeft) {
    const { alert, warning, info } = COLOR_CODES;
    if (timeLeft <= alert.threshold) {
      document
        .getElementById("base-timer-path-remaining")
        .classList.remove(warning.color);
      document
        .getElementById("base-timer-path-remaining")
        .classList.add(alert.color);
    } else if (timeLeft <= warning.threshold) {
      document
        .getElementById("base-timer-path-remaining")
        .classList.remove(info.color);
      document
        .getElementById("base-timer-path-remaining")
        .classList.add(warning.color);
    }
  }

  function calculateTimeFraction() {
    const rawTimeFraction = timeLeft / TIME_LIMIT;
    return rawTimeFraction - (1 / TIME_LIMIT) * (1 - rawTimeFraction);
  }

  function setCircleDasharray() {
    const circleDasharray = `${(
      calculateTimeFraction() * FULL_DASH_ARRAY
    ).toFixed(0)} 283`;
    document
      .getElementById("base-timer-path-remaining")
      .setAttribute("stroke-dasharray", circleDasharray);
  }
}
console.log("Hello World");
//document.getElementById("app").style.display="none";

