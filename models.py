import sqlite3
import os
import os.path
# Connecting to sqlite
# connection object

database = r"hook.db"

def get_conection():
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    return   sqlite3.connect(directory+"/"+database)

def get_cam_by_id(   _cam_id):

     # create a database connection
    conn = get_conection()
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM Camera WHERE id=?", (_cam_id,))

    rows = cur.fetchall()
    return rows[0]
    



def get_clips(  _cam_id):
     
  
    conn = get_conection()
    with conn:    

        cur =  conn.cursor()
        sql_select_query = """SELECT * FROM clips  where camera_id = ? AND isVideo = 1 """
      
        cur.execute(sql_select_query, (_cam_id,))

    records = cur.fetchall()
    return records

def get_clips_bydate(  _start_date, _end_date, _cam_id):
     
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    conn = get_conection()
    with conn:    

        cur =  conn.cursor()
    
        sql_select_query = """ SELECT  * FROM clips where ( cliptime BETWEEN ? AND ? ) AND ( camera_id = ? AND isVideo = 1)"""

      
        cur.execute(sql_select_query, ( _start_date, _end_date, _cam_id))

    records = cur.fetchall()
    print("************************************************************************")
    print(records)
    return records 
       





def get_snaps(  _cam_id):
     
   
    conn = get_conection()
    with conn:    

        cur =  conn.cursor()
        sql_select_query = """SELECT * FROM clips  where camera_id = ? AND isVideo = 0"""

        cur.execute(sql_select_query, (_cam_id,))

    records = cur.fetchall()
    print(records)
    return records    


def get_snaps_bydate(  _start_date, _end_date,_cam_id):
     
   
    conn = get_conection()
    with conn:    

        cur =  conn.cursor()
    
        sql_select_query = """ SELECT  * FROM clips
         where ( cliptime BETWEEN ? AND ? ) AND ( camera_id = ? AND isVideo = 0)"""

      
        cur.execute(sql_select_query, (  _start_date, _end_date, _cam_id))

    records = cur.fetchall()
    return records    

    
################################################################################################

def update_paths(  _camID, _clip_path, _snap_path, _fallback_img , _clip_length, _max_storage):

     data_set=( _clip_path+"/", _snap_path+"/", _fallback_img, _clip_length, _max_storage, _camID )

     conn = get_conection()
     with conn:    

        cur =  conn.cursor()
        cur.execute("UPDATE camera SET clip_folder =?, snapPath = ?, fallbk_image =?, clip_length = ?, max_gbs = ? WHERE id = ?  ",data_set)
        cur.execute("UPDATE tbl_status SET verfied = 0")
     conn.commit()
  
    
     return 1     
##############################################################################################

def insert_clip( _path, _camID,  _dt , _isVideo ,):
     data_set=( _camID, _path,  _dt, _isVideo)
     
     conn = get_conection()
     with conn:    

        cur =  conn.cursor()
        cur.execute("INSERT INTO clips(camera_id, path, cliptime, isVideo) values(?, ?, ?, ?)",data_set)
     conn.commit()
    
    
     return 1


   
def get_clip_by_id(   _clip_id):

     # create a database connection
    conn = get_conection()

    try: 
        with conn: 
            

            cur =  conn.cursor()
            cur.execute("SELECT * FROM clips  WHERE id=?", (_clip_id,))

            rows = cur.fetchall()
            return rows[0]
    except:
        print("Database error")
        return None

def delete_clip( _clip_id):
   
     
   
    conn = get_conection()
    try: 
     with conn:    

        cur =  conn.cursor()
        cur.execute("delete from  clips WHERE id =?", (_clip_id,))
        conn.commit()
        
    
        return 1 
    except:
        print("Database error in delete_clip")
        return -1     








def update_status( _camID, status , _action ):
   
     data_set=( status , _camID, _action) 

     # create a database connection
     conn = get_conection()
     with conn:    

        cur =  conn.cursor()
        cur.execute("UPDATE tbl_status SET status =? WHERE camera_id = ? AND action = ? ",data_set)
     conn.commit()
   
    
     return 1     


def get_status( _cam_id , _action):
     
   
   
    conn = get_conection()
    with conn:    

        cur =  conn.cursor()
        sql_select_query = """SELECT status FROM tbl_status  """
   
        cur.execute(sql_select_query, (_cam_id, _action))


 
    records = cur.fetchall()
    return records[0][0]


def get_all_status( ):
     
    
     # create a database connection
    conn = get_conection()
    with conn:    

        cur =  conn.cursor()
        sql_select_query = """SELECT * FROM tbl_status  """
   
        cur.execute(sql_select_query)


 
    records = cur.fetchall()
    return records   


def get_user_verfication( _cam_id ):
    
    
  
  

    # create a database connection
    conn = get_conection()

    try: 
        with conn: 
            

            cur =  conn.cursor()
            cur.execute("SELECT verfied FROM tbl_status  WHERE camera_id = ?", (_cam_id,))

            rows = cur.fetchall()
            status=rows[0][0]
            return status
    except:
        print("Database error")
        return None


def get_user_verfied( _cam_id , _pass):
    
    
  
  

    # create a database connection
    conn = get_conection()

    try: 
        with conn: 
            

            cur =  conn.cursor()
            cur.execute("SELECT password FROM camera  WHERE id = ?", (_cam_id,))

            rows = cur.fetchall()
            pass_db=rows[0][0]
            if(_pass == str(pass_db)):
              
              update_verf(1,_cam_id)
              return 1
            else:
                return 0


    except:
       print("Database error")
       return None        




def update_verf( _verfied,_camID):
        data_set=( _verfied, _camID) 

     # create a database connection
        conn = get_conection()
        with conn:    

            cur =  conn.cursor()
            cur.execute("UPDATE tbl_status SET verfied = ? WHERE camera_id = ?  ",data_set)
        conn.commit()
        
        
        return 1     
     
    
    
 
def get_is_zoom( _cam_id ):
    
    
  
  

    # create a database connection
    conn = get_conection()

    try: 
        with conn: 
            

            cur =  conn.cursor()
            cur.execute("SELECT zoom_enabled FROM camera  WHERE id = ?", (_cam_id,))

            rows = cur.fetchall()
            status=rows[0][0]
            return status
    except:
        print("Database error")
        return None
   
def delete_Top_Clips( _numbers):
   

    conn = get_conection()
    try: 
     with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT id, path FROM clips   LIMIT ?   ", (_numbers,))
        rows = cur.fetchall()
     
       
        return rows
    except:
        print("Database error")
        return -1     
######################################################################
# 
""" def main():

  rows= delete_Top_Clips(2)
  print(rows)
  for row in rows:
    print(row[0])
       

  
if __name__ == '__main__':
     main()                 """