import webview
import socket

#############################################

def server_program(_window):
    # get the hostname
    print(" socket server started")
    host = socket.gethostname()
    port = 5000  # initiate port no above 1024

    server_socket = socket.socket()  # get instance
    # look closely. The bind() function takes tuple as argument
    server_socket.bind((host, port))  # bind host address and port together
   # _window.on_top = True
   
    _window.move(-100, 900)
   
    _window.hide()
    #(Horizental , vertical)

    # configure how many client the server can listen simultaneously
    server_socket.listen(2)
    conn, address = server_socket.accept()  # accept new connection
    print("Connection from: " + str(address))
    while True:
        # receive data stream. it won't accept data packet greater than 1024 bytes
        data = conn.recv(1024).decode()
        if not data:
            # if data is not received break
            break
        print("from connected user: " + str(data))
        if(str(data) =="Hide"):
            _window.on_top = False
            _window.hide()
            
        if(str(data) =="Show"):
            _window.on_top = True
           # _window.resize(320, 320)
            _window.move(-100, 900)
         
            
            _window.show()
            
           
           
       # data = input(' -> ')
        data = "done"
        
        conn.send(data.encode())  # send data to the client
         

    conn.close()  # close the connection

###################################################################
if __name__ == '__main__':
    # Create a standard webview window
    window = webview.create_window('Slider', 'http://0.0.0.0:10000/slider/', width=600, height=200,   \
         transparent=True,  on_top=True, frameless=True,  hidden=True, easy_drag=True)

    
    webview.start(server_program, window)

""" 
if __name__ == '__main__':
    window = webview.create_window('Hide / show window', 'https://pywebview.flowrl.com/hello', hidden=True)
    webview.start(hide_show, window)
 """