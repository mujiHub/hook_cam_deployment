//alert("From snapshot javascript");


var snapshotbutton = document.getElementById("snapshotbutton");
snapshotbutton.addEventListener("click", snapcallingfunction);
var snapIcon = document.getElementById("snapIcon");
snapIcon.addEventListener("click", snapfunction);
snapIcon.addEventListener("click",  function () {
  
  datafunction("http://0.0.0.0:10000/get_snaps/");
});

function snapcallingfunction() {
  //alert("From snapshot javascript");
  snapshotbutton.addEventListener("click", snapfunction);
  snapshotbutton.addEventListener("click",  function () {
  
    datafunction("http://0.0.0.0:10000/get_snaps/");
  });
}


function snapfunction() {
  //alert("Snapfunction");
  document.getElementById("sensorTab").style.display = "none";
  document.getElementById("videoTab").style.display = "none";
  document.getElementById("snapTab").style.display = "block";

}
function getFileNameFromPath(path) {
  var n = path.lastIndexOf('/');
  return path.substring(n + 1);
}

function datafunction(_url) {
  //alert("From data Function");
  fetch(_url)
    .then(response => response.json())
    .then(data => {
      _renderData(data);

    })
    .catch(error => {

      alert(error);
      console.log(error);
    })
}

function _renderData(data) {
  var id;
      var time;
      var path;
      var result = "";
      var url;
      var sortSnapArray = [];
      var mySnapItem = {};
      // alert("SnapDatafunction");
      data.forEach(element => {
        id = element[0];
        time = element[1];
        path = element[3];
        console.log(path);
        url = "http://0.0.0.0:10000/static/snaps/" + getFileNameFromPath(element[3]);
        console.log(url);


        mySnapItem = {
          Id: id,
          Time: new Date(time),
          Url: url
        }
        sortSnapArray.push(mySnapItem);

        console.log(mySnapItem);
        }
      )

      sortSnapArray.sort(function (a, b) {
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(b.date) - new Date(a.date);
      });
      console.log("---------------------------------------------------------------------");
      console.log("---------------------------------------------------------------------");

      console.log(sortSnapArray);
      var reverseArray = sortSnapArray.reverse();
      console.log(reverseArray);
      var id;
      var time;
      var url;
      reverseArray.forEach(element => {
        id = element.Id;
        time = element.Time;
        url = element.Url;
        console.log("Image url" + url);
        result += "<tr>"
        result += "<td class='py-1'>";
        result += " <input type='checkbox' class='checkMark' id='snapCheck' data-id='" + id + "'>"
        result += " <img  onerror=this.src='../static/no-thumbnail.jpg' src='" + url + "' data-toggle='modal'>";
        result += "</td>";
        result += "<td>";
        result += id;
        result += "</td>";
        result += "<td>";
        result += time;
        result += "</td>";
        result += "<td  id='" + id + "'>";
        result += "<button class='btn-lights' onClick='showImage(event)' data-url='" + url + "'>View</button>";
        result += "<button class='btn-lights'id ='copybtn' onClick ='snapcopyfunction(event)'>Copy</button>";
        result += "<button class='btn-lights' id='archivebtn' onclick ='snaparchivefunction(event)'>Archive</button>";
        result += "</td>";
        result += "</tr>";
      })

      snapOutput.innerHTML = "";
      snapOutput.innerHTML = result;
}



function filterSnapData() {
  var startDate = new Date(document.getElementById("startDate").value);
  var endDate = new Date(document.getElementById("endDate").value);


    if (startDate == "Invalid Date" || endDate == "Invalid Date") {
      return alert("Select Valid date")
    }
    else if (startDate>endDate) {
      return alert("Start Date is Greater then End date");
    }

    var utc_timestamp_start = Date.UTC(startDate.getUTCFullYear(),startDate.getUTCMonth(), startDate.getUTCDate() , 
    startDate.getUTCHours(), startDate.getUTCMinutes(), startDate.getUTCSeconds(), startDate.getUTCMilliseconds());

    var utc_timestamp_end = Date.UTC(endDate.getUTCFullYear(),endDate.getUTCMonth(), endDate.getUTCDate() , 
    endDate.getUTCHours(), endDate.getUTCMinutes(), endDate.getUTCSeconds(), endDate.getUTCMilliseconds());

    var _filter_url = 'http://0.0.0.0:10000/filter_snaps/'+utc_timestamp_start+'/'+utc_timestamp_end ;
    
    datafunction(_filter_url);

}


function httpPost(theUrl) {
  let xmlHttpReq = new XMLHttpRequest();
  xmlHttpReq.open("Get", theUrl, false);
  xmlHttpReq.send(null);

  return xmlHttpReq.responseText;
}








function showImage(event) {
  var url = $(event.target).attr("data-url");
  var idClicked = $(event.target).parent().attr('id');
  console.log(idClicked);
  console.log("From show Image" + url);
  $("#camImage").attr("src", url);
  var pathhhh = $("#camImage")[0].getAttribute("src");
  console.log(pathhhh);
  $('.modl-footer').attr('id', idClicked);
  $('#imageModel').modal('show');
  //  $("#camImage").html('<src="' + url + '" >');
  // var imgPath =$(this).find("#camImg").attr("src");
  //alert(imgPath);
  // console.log(imgPath);
  //$('#camImage').attr('src',$(this).attr(url));


}





function getSelectedIds() {
  var checkedVals = [];
  checkedVals = $('.checkMark:checkbox:checked').map(function () {
    return $(this).attr("data-id");

  })
    .get();

  //console.log("checked values array"+ checkedVals);
  //return(checkedVals);
  //return parseInt(checkedVals);
  //var a = parseInt(checkedVals);
  //return parseInt(a);
  var arrofInt = [];
  checkedVals.map(str => {
    arrofInt.push(Number(str));
  })
  console.log(arrofInt);
  return (arrofInt);
}


function snapcheckfunction() {
  if ($("#snapCheckbox").is(":checked")) {
    $(".checkMark").show();
    $("#snapcheckboxButton").show();

  }
  else {
    $(".checkMark").hide();
    $("#snapcheckboxButton").hide();
  }
}



function snapcopyfunction(event) {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  idClicked = $(event.target).parent().attr('id');
  console.log(idClicked);
  var postval = 'http://0.0.0.0:10000/copy/' + idClicked;
  console.log(httpGet(postval));

}


function snapcopyMultiple() {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  var multipleIds = (getSelectedIds());
  console.log(multipleIds);
  var postval = 'http://0.0.0.0:10000/copy_multiple/' + multipleIds;
  console.log(httpGet(postval));
  console.log(multipleIds);
}

function snaparchivefunction(event) {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  var idClicked = $(event.target).parent().attr('id');
  console.log(idClicked);
  var postval = 'http://0.0.0.0:10000/archive/' + idClicked;
  console.log(httpGet(postval));

}




function snaparchiveMultiple() {
  function httpGet(theUrl) {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.send(null);

    return xmlHttpReq.responseText;
  }
  var multipleIds = (getSelectedIds());
  var postval = 'http://0.0.0.0:10000/archive_multiple/' + multipleIds;
  console.log(httpGet(postval));
  console.log(multipleIds);
}








//UTC timezone.
//Epoch Timestamp
//Timezones.
//DAte.UTC javascript 







//$('body').plainOverlay('show', {
  //show: function(event) {
    // Now, the overlay is shown. The user can't scroll the page.
    // Do something...
    // Scroll the page to show something.
    //$('body').plainOverlay('scrollLeft', 100).plainOverlay('scrollTop', 400)
      // And hide the overlay.
     // .plainOverlay('hide');
  //}
//});









/*var overlay = jQuery('div id="overlay"></div>');
overlay.appendTo(document.body);
console.log("overlay .............");*/


/*$('#show-button').click(function() {
  alert("overlay");
  // Same initializing per every showing
  $('#list').plainOverlay('show', {duration: 500});
});*/