import pickle
import time
import datetime


from sqlalchemy import false, true
from models import *
import threading
import sys
import os
import os.path
import glob

import gi
from gi.repository import Gtk
gi.require_version('Gst', '1.0')
#from gi.repository import Gst, GObject
from gi.repository import Gst, GLib, GstVideo, GObject


from app_settings import *

from snif import *


status_recording = False
Exit_recording = False

###########################################################________________________________#####################################################
def snif_stream(_self):
      print('Starting a  SNIFFER..')
      loop_on = True
      while loop_on:
        time.sleep(4)
        print("\n SNIF URL"+_self.rtsp.strip())
        ret= get_rtsp_status(_self.rtsp.strip())
        print("\n sniffer response is "+str(ret))
        if(ret==1):
            _self.connect_Ip_cam()
            print("Sniffer Exit")
            loop_on = False
            print("\n ###################  Stream FOUND EXITING SNIFF ##################### \n")

#######################################################


def getDirSize(_self):
    # get size
            size = 0
            gbs = 0
            for path, dirs, files in os.walk(_self.folder_rec ):
                for f in files:
                    fp = os.path.join(path, f)
                    size += os.stat(fp).st_size
                    if(size == 0):
                      size=10
                    gbs=round(size / (1024 * 1024 * 1024), 3)

            print('folderSize  size: ' + str(gbs) + ' GBs')
            return gbs

def  TH_folder_check( _self):


        while  Exit_recording is False :

            while(getDirSize(_self) > _self.max_gbs):
                rows = delete_Top_Clips(3)

                for path_del in rows:
                    db_id= path_del[0]
                    filePath = path_del[1]
                    dirpath = os.path.dirname(os.path.realpath(filePath))
                    if(os.path.exists(filePath)):
                     os.remove(filePath)
                     delete_clip(db_id)
                     thunmb_name = os.path.splitext(os.path.basename(filePath))[0]
                     thunmb_path = dirpath+"/thumbnails/"+thunmb_name+".jpg"
                     if(os.path.exists(thunmb_path)):
                      os.remove(thunmb_path)
                print("TOP 3 deleted ")
            time.sleep(30)

            



def  TH_udpRecord( _self):
    global status_recording
    status_recording = true 
    pipe_udp = None
    clip_name = None
    thumb_name = None
    duration = 1
    str_rec_pipe = None
    RecPipeLine = None
    folderTH = threading.Thread(target=TH_folder_check, args=(_self,))
    folderTH.start()
    
    bus_rec = _self.BasePipeLine.get_bus()

    try:

        while Exit_recording is False  :

                            msg = bus_rec.timed_pop_filtered(int(duration) * Gst.SECOND, ( Gst.MessageType.ERROR))
                            duration = _self.clip_length
                            clip_time =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
                    
                            if RecPipeLine is not None:                          
                                RecPipeLine.send_event(Gst.Event.new_eos())
                                RecPipeLine.set_state(Gst.State.NULL)
                                if(clip_name is not None):
                                  if(os.path.getsize(clip_name)==0):
                                   os.unlink(clip_name)
                                  else:
                                      insert_clip( clip_name,  _self.cam_id, clip_time , 1 )
                                      thumb_path=_self.folder_rec+"thumbnails/"+thumb_name+".jpg"
                                      #generate_thumbnail(clip_name, thumb_path, 0.1, 120) 
                                      snapTh= threading.Thread(target=TH_Thumbnail, args=(clip_name, thumb_path), daemon=True)
                                      snapTh.start()
                                  
    
                                      

                            thumb_name = _self.get_name()
                            clip_name =  _self.folder_rec+thumb_name +".mp4"
                            print("recording in loop \n  clip_name "+clip_name.strip() )    
                          
                            str_rec_pipe = "udpsrc  port="+_self.udp_port+" ! application/x-rtp, clock-rate=90000,payload=96  \
                                 ! rtph264depay ! h264parse  ! avdec_h264 ! videoconvert ! x264enc pass=quant ! video/x-h264, profile=baseline  ! matroskamux streamable=false ! \
                                filesink sync=False location="+clip_name.strip()
                            


                            RecPipeLine  = Gst.parse_launch(str_rec_pipe  )
                            ret = RecPipeLine.set_state(Gst.State.PLAYING)
                            if ret == Gst.StateChangeReturn.FAILURE:
                                 print("\n ##################  ERROR : Unable to Record")  
    finally:
            if RecPipeLine is not None:                          
                RecPipeLine.send_event(Gst.Event.new_eos())
                RecPipeLine.set_state(Gst.State.NULL)                   
            status_recording = False 
            update_status( _self.cam_id , 0, "record" )
            if(os.path.getsize(clip_name)==0):
                                   os.unlink(clip_name)
            folderTH.join()
            print("\n ##################  Exited :Record Thread")               

        

##################################################Thread to listen on pipe from web API #################################
#StartRecord(self, _fName, _fDescp):

import socket
def socketServer(_self):
    HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
    PORT = 65432  # Port to listen on (non-privileged ports are > 1023)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()
        with conn:
            print(f"Connected by {addr}")
            while True:
                data = conn.recv(1024)
                print ("on_server \n")
                msg = data.decode()
                process_msg(msg, _self)
                print(data)
                print ("on_server \n")
                if not data:
                    break
                conn.sendall("Done".encode("utf-8") )


def process_msg(_msg, _self):

    word=_msg.split(',')

    if word[0] == "Snap":
        print ("Snap  pipe asked " )
        _self.Snap()

    if word[0] == "Stop":
        print ("Stop  pipe asked " )
        global Exit_recording
        Exit_recording = True
        update_status( _self.cam_id , 0, "record" )


    if word[0] == "Record":
        print ("\n Record  pipe asked >>>>>>>> " )
        _self.Start_Record()

    if word[0] == "Flip":
        if(_self.FlipAngle == "0"): 
            _self.FlipAngle = "clockwise"
        elif(_self.FlipAngle  == "clockwise"): 
            _self.FlipAngle = "rotate-180"
        elif(_self.FlipAngle  == "rotate-180"):
             _self.FlipAngle = "counterclockwise"  
        elif(_self.FlipAngle  == "counterclockwise"):
             _self.FlipAngle = "0"         
        _self.connect_Ip_cam()    

                 

###################################################################################################################################

def  TH_playFile( _self):
     #PlayerUI("/home/muji/Dev/rec_video/sample.mp4")
     print("TH_playFile( _self):")
    # PlayerUI("/home/muji/Staged/HookCam_stage/static/videos/cam_pi_3_7212022-122718.mp4")
     

             


############################################################################################


class Camera():

    def __init__(self, _id, _xid,):

        threading.Thread.__init__(self)
        Gst.init(None)

        self.row = get_cam_by_id(_id)
       
        self.cam_id = self.row[0]
       # self.close()
        self.name = self.row[1].strip()
        self.clip_length=self.row[5]
        self.rtsp =self.row[2].strip()
        self.folder_rec = self.row[4].strip()
        self.fileName = self.row[6].strip()
        self.cam_user = self.row[8].strip()
        self.cam_pass = self.row[9]
        self.cam_ip = self.row[10].strip()
        self.snapPath = self.row[11].strip()
        self.fb_image_path=self.row[12].strip()
        self.max_gbs=self.row[13]
        self.isZoom=self.row[14]



        self.State = None
        self.isFallBack = False
        self.recTH= None
        self.snapTH = None
        ######  Flip the video 
        self.FlipAngle = "0"



        self.live_xid = _xid
        
        self.BasePipeLine = None
        self.FallBackLine = None
        self.recordpipe = None
        self.loopTh = None
        self.udpTh = None
        self.udp_port = str(5000 + self.cam_id)
       
        self.ip_cam_on_staus = False 

        self.image_url = 'filesrc location=connecting.png ! decodebin ! videoconvert ! imagefreeze ! autovideosink'



        self.state = Gst.State.NULL
       # glimagesink rotate-method=1
        #rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4

        self.Main_url = None
        """ 
        ### Flip vidoe 

gst-launch-1.0 -v v4l2src ! tee name=t \
    t. ! queue ! videoscale ! video/x-raw,width=640,height=480 ! \
        videoconvert ! autovideosink \
    t. ! queue ! videoscale ! video/x-raw,width=360,height=240 ! \
    videoflip method=clockwise ! \
        videoconvert ! autovideosink
###############################################################################################

                strH265 = ' rtspsrc location=' + self.rtsp + ' drop-on-latency=1 latency=100 !  \
                    queue max-size-buffers=0 max-size-time=0 max-size-bytes=0 min-threshold-time=10  !    \
                        rtpjitterbuffer latency=100 ! rtph265depay  ! h265parse !  avdec_h265  !   videoconvert !  \
                            tee name=tpng !   tee name=Tu !  tee name=Tr ! queue name=dispqueue leaky=1 ! autovideosink '
            
                self.FallBack_url = 'fallbacksrc uri=https://www.wrong.webm timeout=1000 fallback-uri=file://'+ self.fb_image_path +' ! videoconvert ! autovideosink'


                gst-launch-1.0 rtspsrc location=rtsp://127.0.0.1:8554/test latency=10 ! decodebin ! autovideosink

                gst-discoverer-1.0 rtsp://127.0.0.1:8554/test

                 gst-launch-1.0 -v filesrc location=demo1.png ! decodebin ! videoconvert ! imagefreeze ! autovideosink

                 sudo nano /lib/systemd/system/hk.service

        """
        self.FallBack_url =  "filesrc location=/home/muji/Staged/HookCam_stage/flb.mp4 \
  ! qtdemux \
  ! decodebin ! videoconvert ! videoscale ! autovideosink "
        #self.connect_Ip_cam()
        #self.connect_image()
        self.fallback()


 
    def  connect_Ip_cam(self ): 

        if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)

        self.ip_cam_on_staus = True 
        if(self.FlipAngle != "0"):
         self.Main_url = ' rtspsrc location=' + self.rtsp + ' drop-on-latency=1 latency=100 !  \
            queue max-size-buffers=0 max-size-time=0 max-size-bytes=0 min-threshold-time=10  !    \
                rtpjitterbuffer latency=100 ! rtph264depay  ! h264parse !  avdec_h264  !   videoconvert !  \
                    tee name=tpng !   tee name=Tu !  tee name=Tr ! queue name=dispqueue leaky=1 !    videoflip method='+self.FlipAngle +' ! autovideosink  '
        else :
            self.Main_url = ' rtspsrc location=' + self.rtsp + ' drop-on-latency=1 latency=100 !  \
            queue max-size-buffers=0 max-size-time=0 max-size-bytes=0 min-threshold-time=10  !    \
                rtpjitterbuffer latency=100 ! rtph264depay  ! h264parse !  avdec_h264  !   videoconvert !  \
                    tee name=tpng !   tee name=Tu !  tee name=Tr ! queue name=dispqueue leaky=1 !   \
                          autovideosink  '            

        """ 
videoscale \
! "video/x-raw,height=1080"


gst-launch-1.0 \
videotestsrc ! video/x-raw,width=1280,height=720 \
! videoscaleratio height=1080 \
! xvimagesink
 """

        self.connect(self.Main_url )

    def  connect_image(self ): 
        
        if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)

        self.ip_cam_on_staus = False 
        self.connect(self.image_url )  







    def  connect(self, _pipeline):


         
            self.BasePipeLine = Gst.parse_launch(_pipeline )
            bus = self.BasePipeLine.get_bus()
            bus.add_signal_watch()
            bus.enable_sync_message_emission()
            bus.connect('sync-message::element', self.on_cam_sync_message)
            bus.connect("message", self.on_message)
            ret = self.BasePipeLine.set_state(Gst.State.PLAYING)
            if ret == Gst.StateChangeReturn.FAILURE:
                print("\n ##################  ERROR : Unable to set the pipeline to the playing state")
                #sys.exit(1)
                self.fallback()
           # else:
                #   if(self.loopTh is not  None ):
                  #  self.isFallBack = False
            self.State = Gst.State.PLAYING
           
      





      

   


    def  fallback(self):

            self.isFallBack = True
            if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)
            if(self.ip_cam_on_staus):
              self.ip_cam_on_staus = False
              if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)
              print("ON FALL BACK >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ##################################### /n")
            self.ip_cam_on_staus = False 
            self.connect(self.image_url )  
           
            print("\n This is Fallback Stream ON NOW ")
            self.loopTh= threading.Thread(target=snif_stream, args=(self,), daemon=True)
            self.loopTh.start()
          






    def on_cam_sync_message(self, bus, msg):
        if msg.get_structure().get_name() == 'prepare-window-handle':
            print('Live Window handle Set Done')
            msg.src.set_window_handle(self.live_xid)



        # #################

    def on_message(self, bus, message):
        t = message.type

        if t == Gst.MessageType.ERROR:
            err, dbg = message.parse_error()
            update_status( self.cam_id , 0, "live_stream" )
            
            
            print("\n ERROR:   >>>>>>>>> ", message.src.get_name(), ":", err)
            self.fallback()
           

            if dbg:
                print("Debug info:", dbg)
            self.terminate = True
         #GstRTSPSrcTimeout
        elif t == Gst.MessageType.ELEMENT:
            #print("\n  Gst.MessageType.ELEMENT   >>>>>>>>> ")
            name = message.get_structure().get_name()
            res = message.get_structure()
           
            source = message.src.get_name()  # (str(message.src)).split(":")[2].split(" ")[0]
             #You need to explicitly set elements to the NULL state before
            if(name=="GstRTSPSrcTimeout"):
                 print("Time OUT")
                 global Exit_recording
                 Exit_recording = True
                 
                 self.BasePipeLine.set_state(Gst.State.NULL)
                 self.fallback()
               
            #print("name = "+str(name) +"\n res = "+str(res) +"\n source = "+str(source))

        elif t == Gst.MessageType.EOS:
            update_status( self.cam_id , 0, "live_stream" )
            print("\n End-Of-Stream reached >>>>>>>>>>>>>>>>>>>> ")
            self.fallback()
           
            self.terminate = True

            #########################################################################################################################===== STREAM start ++++++++++++++##############
        elif t == Gst.MessageType.STREAM_START:
                 self.isFallBack=False
                 if(self.FallBackLine is not None):
                  self.FallBackLine.set_state(Gst.State.NULL)
                 print("\n Camera is UP Now")
                 if(self.ip_cam_on_staus):
                    update_status( self.cam_id , 1, "live_stream" )
                    pipe_udp =  Gst.parse_bin_from_description("queue name=udpQ  !  x264enc tune=zerolatency    \
                        ! rtph264pay ! udpsink host=127.0.0.1 port="+self.udp_port , True)
                    self.BasePipeLine.add(pipe_udp)              
                    self.BasePipeLine.get_by_name("Tu").link(pipe_udp)    
                    self.State = Gst.State.PLAYING
                    ret = pipe_udp.set_state(Gst.State.PLAYING)
                    if ret == Gst.StateChangeReturn.FAILURE: 
                        print("ERROR: Unable to set the Unable to send on POERT "+self.udp_port)
                    else:
                        print("\n UDP push started on "+self.udp_port)
                    self.threadLoop= threading.Thread(target=socketServer, args=( self,))
                    self.threadLoop.start()
                     #self.display_settings() ######### Display setting windows 


             #print("\n ###############      MESSAGE_STREAM_START           ####### \n ")
             #msg_q.put(1)
        elif t == Gst.MessageType.DURATION_CHANGED:
            # the duration has changed, invalidate the current one
            self.duration = Gst.CLOCK_TIME_NONE
        elif t == Gst.MessageType.STATE_CHANGED:
            old_state, new_state, pending_state = message.parse_state_changed()
        





    ###-------------------------------------------------------************************************__________________________________###




           #scp  camera.py pi@192.168.100.11:/home/pi/Documents/utzo/src/
           #gst-launch-1.0 playbin uri=file:///home/pi/Videos/films/piCam1.mp4

    def get_name(self):
      """ Return a string of the form yyyy-mm-dd-hms """
      from datetime import datetime
      today = datetime.today()
      y = str(today.year)
      m = str(today.month)
      d = str(today.day)
      h = str(today.hour)
      mi= str(today.minute)
      s = str(today.second)
      #date(yyyy, mm, dd)
      #date = datetime.date(2018, 5, 12)
     # tm1 = '%%s%%s%%%%s' %( m, d, y)
     # tm2= '-%%s%%s%%s' %( h, mi, s)
      fname = self.fileName+"_"+str(self.cam_id)+"_"+m+d+y+"-"+h+mi+s
#Mm dd yyyy
      return   fname.strip() #'%s-%s-%s-%s%s%s' %(y, m, d, h, mi, s)







    def Snap(self):


       # _button.set_sensitive(False) # true to enable the button

        self.snapTH = threading.Thread(target=TH_snap, args=(self,))
        self.snapTH.start()
        self.snapTH.join()
       # _button.set_sensitive(True)
        self.snapTH = None


   
      


    def Start_Record(self):
          
          global Exit_recording
            
          if(status_recording is False ):
            Exit_recording = False
            self.recTh= threading.Thread(target=TH_udpRecord, args=(self,), daemon=True)
            self.recTh.start()
            update_status( self.cam_id , 1, "record" )
        #  self.rtmpTh= threading.Thread(target=TH_rtmp, args=(self,), daemon=True)
        #  self.rtmpTh.start()

          return 1

       





    def close(self):
        update_status( self.cam_id , 0, "live_stream" )
        update_status( self.cam_id , 0, "record" )


        if(self.recTH is not None):
              self.recTH.join()
        if(self.snapTH is not None):
              self.snapTH.join()
        if( self.BasePipeLine is not None):
           self.BasePipeLine.set_state(Gst.State.NULL)
        update_status( self.cam_id , 0, "record" )
       
   
        
        print("\n ################# Program Ended  ##################")






    def probe_block(self, pad, buf):
            print('recording probe_block >')
            return True








##################################################Thread to Snap Image #################################

def  TH_snap( _self):
        pipeSnap = None
        imgqueue = None
        snapName= _self.snapPath +_self.get_name()+".jpg"
   
        clip_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

        try:

         if(_self.State == Gst.State.PLAYING):
             pipeSnap = Gst.parse_bin_from_description("queue name=imgqueue leaky=2 ! jpegenc ! filesink location="+snapName ,True)

             # Add Bin To Line
             if( _self.BasePipeLine is not None):
               _self.BasePipeLine.add(pipeSnap)
               _self.BasePipeLine.get_by_name("tpng").link(pipeSnap)
               ret = pipeSnap.set_state(Gst.State.PLAYING)
               time.sleep(2)
               #clean after Image capture
               imgqueue =  pipeSnap.get_by_name("imgqueue")
               imgqueue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, _self.probe_block)
               _self.BasePipeLine.get_by_name("tpng").unlink(pipeSnap)
               imgqueue.get_static_pad("sink").send_event(Gst.Event.new_eos())
               if ret == Gst.StateChangeReturn.FAILURE:
                            print("ERROR: Unable to Start Recording")
                            _self.TerminateRecording=True
               else:
                insert_clip( snapName, _self.cam_id , clip_time, 0 )

              # pipeSnap.set_state(Gst.State.PAUSED)

               #pipeSnap  = None

        finally:


            if pipeSnap is not None:

                # Block new data
                filequeue = pipeSnap.get_by_name("imgqueue")
                filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, _self.probe_block)

                # Disconnect the recording pipe
                _self.BasePipeLine.get_by_name("tpng").unlink(pipeSnap)

                # Send a termination event to trigger the save
                filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())

                # Clear the reference to the pipe
               # pipeSnap.set_state(Gst.State.NULL)
                pipeSnap = None
                print("\n finally: Captured exited \n")







############################################################################################

# video/x-raw, format=I420, width=400, height=400, framerate=1/5 !
##################################################Thread to Snap Image #################################
#gst-launch-1.0 filesrc location=test.mp4 ! decodebin ! videoconvert ! pngenc snapshot=true ! filesink location=frame.png
#gst-launch-1.0 videotestsrc ! video/x-raw, format=I420, width=400, height=400, framerate=1/5 ! identity sync=true ! timeoverlay ! \
# jpegenc ! multifilesink location="img_%06.jpg"

######################
#gst-launch-1.0 -e videotestsrc ! video/x-raw, format=I420, width=100, height=100, framerate=1/5 ! identity sync=true ! \
 #jpegenc ! filesink location="img_1.jpg"
#################
# gst-launch-1.0 videotestsrc ! video/x-raw, format=I420, width=200, height=100 ! pngenc snapshot=true ! filesink location=frame.png

""" 
    pipeline = Gst.parse_launch(
        "filesrc location="+_video_file + "\
            ! decodebin ! videoconvert ! pngenc snapshot=true ! \
            filesink location= "+_snap_file
    )


    gst-launch-1.0 -e videotestsrc ! video/x-raw, format=I420, width=100, height=100, framerate=1/5 ! identity sync=true ! \
 jpegenc ! filesink location="img_1.jpg"


 gst-launch-1.0 -e videotestsrc ! video/x-raw, format=I420, width=100, height=100 ! \
 jpegenc ! filesink location="img_2.jpg"
 """

def  TH_Thumbnail( _video_file, _snap_file):
    
    
        
    pipeline = Gst.parse_launch(
        "filesrc location="+_video_file + "\
            ! video/x-raw, format=I420, width=200, height=150 ! decodebin ! videoconvert  ! jpegenc ! \
            filesink location= "+_snap_file
    )

    # start playing
    pipeline.set_state(Gst.State.PLAYING)

    # wait until EOS or error
    bus = pipeline.get_bus()
    msg = bus.timed_pop_filtered(
        Gst.CLOCK_TIME_NONE,
        Gst.MessageType.ERROR | Gst.MessageType.EOS
    )

    # free resources
    pipeline.set_state(Gst.State.NULL)







############################################################################################